//! Clock implementation based on the Rust standard library

use crate::{Clock, Instant};
use std::convert::TryInto;

/// Clock implementation based on the Rust standard library
///
/// Guaranteed to be there on any system supported by the Rust standard library
/// and to be monotonic (never jump back in time), but durations may inflate or
/// shrink during clock calibration events (e.g. NTP synchronization), and there
/// are no guarantee about precision or correct suspend handling.
///
pub struct StdClock(std::time::Instant);

impl Default for StdClock {
    fn default() -> Self {
        Self(std::time::Instant::now())
    }
}

impl Clock for StdClock {
    fn now(&self) -> Instant {
        let elapsed_nanos = self.0.elapsed().as_nanos();
        let elapsed_nanos: i64 = elapsed_nanos
            .try_into()
            .expect("Either ~300 years have elapsed or std::time has a bug!");
        Instant(elapsed_nanos)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn std_clock() {
        crate::clocks::tests::generic_clock_test::<super::StdClock>();
    }
}
