//! Standard imports to make this crate easiest to use

pub use crate::Clock;

#[cfg(target_os = "linux")]
pub use crate::clocks::linux::PosixClock;
