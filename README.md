# sched-clock: A timer for task scheduling

[![On crates.io](https://img.shields.io/crates/v/sched-clock.svg)](https://crates.io/crates/sched-clock)
[![On docs.rs](https://docs.rs/sched-clock/badge.svg)](https://docs.rs/sched-clock/)
![Requires rustc 1.34+](https://img.shields.io/badge/rustc-1.34+-red.svg)

## What is this?

So, you want to do [task scheduling](https://en.wikipedia.org/wiki/Scheduling_(computing)),
and in this context you need to reason about sentences such as:

- This task must be done in 16.7ms
- This task will take approximately 5ms
- How long do I typically take to service tasks?
- How much time have I spent servicing this task so far?

You do not need to reason about historical events that occured more than 200
years ago. And you do not need to plan ahead more than 200 years in the future
because you're as convinced as I am that your software (like the rest of
computing as we know it) will be obsolete by that time.

You do not need often to correlate timestamps with an UTC reference (like the
Unix epoch), and you are aware that these "global" reference clocks are anyway
not really global, full of messy metrology edge cases, and overall best avoided
unless one really needs them (e.g. for direct human interaction).

On the other hand, you care about precise timing (at least microsecond,
nanosecond would be nice) and keeping data structures small and timing-related
measurements/manipulations fast.

If all of the above sounds right, this library might be for you!

## Features

- 64-bit, signed, monotonic, nanosecond-based timestamps and durations
- Tries to handle all those edge cases that make clocks fun to implement:
    * Thread migration across CPU cores
    * CPU frequency changes
    * Suspend and resume
    * User adjusting the local time
    * NTP speeding up and slowing down your system clock
    * Bugs in the hardware clock
- As portable as Rust stdlib, but may have optimized paths for specific systems
